import React,{Component} from "react";
import Footer from "./components/static/Footer";
import Header from "./components/static/Header";
import Home from "./components/pages/Home";
import styles from "./App.css";

export default class App extends Component{
    render() {
        return(
        <div className="app-container">
            <Header/>
            <Home/>
            <Footer/>
        </div>
        )
    }
}